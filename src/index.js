import React from 'react';
import ReactDOM from 'react-dom';

import App from './app';
import GameContext from './context/gameContext.js';
import ScoreContext from './context/scoreContext.js';
import './index.css';

ReactDOM.render(
  <GameContext>
    <ScoreContext>
      <App />
    </ScoreContext>
  </GameContext>,
  document.getElementById('root')
);
